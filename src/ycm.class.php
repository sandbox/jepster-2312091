<?php

use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Parser;

interface ycmInterface {

	public function write_yaml_file_per_node_for_all_nodes();

	public function create_nodes_from_all_yaml_files();

}

class ycm implements ycmInterface {

	public function __construct($path_to_yml_files = false){
		if($path_to_yml_files === false){
			$this->path_to_yml_files = drupal_realpath('public://') . "/yaml_content_migrate/";
		} else {
			$this->path_to_yml_files = $path_to_yml_files;
		}
	}

	public function write_yaml_file_per_node_for_all_nodes(){
		$result = db_query('SELECT n.nid FROM {node} n');

		// Result is returned as an iterable object that returns a stdClass object on each iteration
		foreach ($result as $record) {
			$node = node_load($record->nid);
			$node_array = (array) $node;

			// @TODO: implement here api for invoking other modules field types with attachments

            /**** FILE ID IS FETCHED INDEPENDENT FROM ANY SPECIFIC FIELD NAME - BEGIN ****/
            foreach($node_array as $key=>$value){
                #$field = field_info_field($key);
                if(is_array($value) && $this->array_key_exists_recursive("fid", $value)){
                    $arr__file_field = field_get_items("node", $node, $key);
                    $arr__file_field_value = field_view_value("node", $node, $key, $arr__file_field["0"]);
                    $fid = $arr__file_field_value["#item"]["fid"];
                    $file = file_load($fid);
                    $real_file_path = drupal_realpath($file->uri);
                    if(!copy($real_file_path, drupal_realpath('public://') . "/yaml_content_migrate/img/" . $file->filename)){
                        throw new Exception("Image file couldn't be copied");
                    }
                }
            }
            /**** FILE ID IS FETCHED INDEPENDENT FROM ANY SPECIFIC FIELD NAME - END ****/

            $dumper = new Dumper();
            $yaml_node = $dumper->dump($node_array, 2);
			file_put_contents($this->path_to_yml_files . $node->title . ".yml", $yaml_node);
		}

	}

	public function create_nodes_from_all_yaml_files(){
	  if ($handle = opendir($this->path_to_yml_files)) {
	    while (false !== ($entry = readdir($handle))) {

	      if(file_exists($this->path_to_yml_files . $entry) &&
	        is_file($this->path_to_yml_files . $entry)){

              #$the_file_content = yaml_parse_file($this->path_to_yml_files . $entry);

              $yaml = new Parser();
              $the_file_content = $yaml->parse(file_get_contents($this->path_to_yml_files . $entry));

	          foreach($the_file_content as $key=>$value){
	              $node[$key] = $value;
	          }
	          // to create a new node
	          unset($node["nid"]);
	          unset($node["vid"]);

              try {
                  node_save((object) $node);
              } catch (Exception $e) {
                  echo 'Caught exception: ',  $e->getMessage(), "\n";
              }

	      }
	    }
	  }
	}

    private function array_key_exists_recursive($needle, $haystack){
        $result = array_key_exists($needle, $haystack);
        if ($result) return $result;
        foreach ($haystack as $v) {
            if (is_array($v)) {
                $result = $this->array_key_exists_recursive($needle, $v);
            }
            if ($result) return $result;
        }
        return $result;
    }

}