<?php

class ycm_test extends PHPUnit_Framework_TestCase {

	public $nodesArrayFromYamlFilesToCompare = array();
	public $path_to_yml_files;

	function __construct() {
		// boot drupal
		$this->defineDrupalRoot();
		include_once(DRUPAL_ROOT . "/includes/bootstrap.inc");
		$_SERVER['REMOTE_ADDR'] = "PHPUnit";
		drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
		include_once(drupal_get_path("module", "yaml_content_migrate") . "/src/ycm.class.php");
		$this->path_to_yml_files = drupal_get_path("module", "yaml_content_migrate") . "/test_fixtures";
	}


	public function defineDrupalRoot(){
		/* iterate directories down in hierarchy, until drupal's index.php and xmlrpc.php will be found.
		   this files indicate the drupal root directory and getcwd(); gives drupal's root directory. */
	    if ($handle = opendir(getcwd())) {
		    while(!file_exists(getcwd() . "/index.php")
		    	&& !file_exists(getcwd() . "/xmlrpc.php")){
		    	chdir("../");
			}
	  	}
	  	define('DRUPAL_ROOT', getcwd());
	}

	public function createCompareArrayFromYamlFiles(){

		if ($handle = opendir($this->path_to_yml_files)) {
		    while (false !== ($entry = readdir($handle))) {
		      if(file_exists($this->path_to_yml_files . "/" . $entry) &&
		        is_file($this->path_to_yml_files . "/" . $entry)){
		          $the_file_content = yaml_parse_file($this->path_to_yml_files . "/" . $entry);
          
		          foreach($the_file_content as $key=>$value){
		              $node[$key] = $value;
		          }
		          // to create a new node
		          unset($node["nid"]);
		          unset($node["vid"]);

		          $this->nodesArrayFromYamlFilesToCompare[] = $node;
		      }
		    }
		}
		return $this->nodesArrayFromYamlFilesToCompare;
	}

	public function testIfNodeImportWorks() {
		//var_dump(node_load(91));
		$ycm = new ycm(drupal_get_path("module", "yaml_content_migrate") . "/test_fixtures/");
		$ycm->create_nodes_from_all_yaml_files();
		
		$nodes_in_drupal_db = entity_load('node');
		$nodes_from_YAML = $this->createCompareArrayFromYamlFiles();

		$saved_nodes = array();
		foreach($nodes_in_drupal_db as $node_in_drupal_db){
			array_push($saved_nodes, $node_in_drupal_db->title);
		}

		// simple comparison with node title
		foreach($nodes_from_YAML as $node_from_YAML){
			$this->assertContains($node_from_YAML["title"], $saved_nodes);
		}
		
	}

	function __destruct() {
		// delete all nodes imported for test
		$nodes_in_drupal_db = entity_load('node');
		foreach($nodes_in_drupal_db as $node_in_drupal_db){
			if(is_numeric(strpos($node_in_drupal_db->title, "PHPUnit Test")) ){
				node_delete($node_in_drupal_db->nid);
			}
		}
	}

}
